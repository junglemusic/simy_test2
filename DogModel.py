from flask import Flask
import json
from settings import app
from flask_sqlalchemy import SQLAlchemy
from cachetools import cached, TTLCache

cache = TTLCache(maxsize=app.config['CACHE_DEFAULT_MEM_SIZE'], ttl=app.config['CACHE_DEFAULT_EXP'])
db = SQLAlchemy(app)

invalid_object_helper_string = {"error": "Invalid object",
                                  "helpString": "JSON looks like : {'name': 'Rufus', 'breed': 'Elsatian', 'gender': 'male', 'height': 54.2, 'weight': 24.5}"}


class Dog(db.Model):
    __tablename__ = 'dog'
    id = db.Column(db.String(80), primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    breed = db.Column(db.String(80), nullable=False)
    height = db.Column(db.Float, nullable=False)
    weight = db.Column(db.Float, nullable=False)
    gender = db.Column(db.String(45), nullable=False)

    def json(self):
        return {'id': self.id, 'name': self.name, 'breed': self.breed, 'height': self.height, 'weight': self.weight,
                'gender': self.gender}

    def add_dog(_id, _name, _breed, _height, _weight, _gender):
        new_dog = Dog(id=_id, name=_name, breed=_breed, height=_height, weight=_weight, gender=_gender)
        db.session.add(new_dog)
        db.session.commit()

    def update_dog(_dog):
        dog_to_update = Dog.query.filter_by(id=_dog["id"]).first()
        if not dog_to_update:
            return False

        dog_to_update.name = _dog["name"]
        dog_to_update.breed = _dog["breed"]
        dog_to_update.height = _dog["height"]
        dog_to_update.weight = _dog["weight"]
        dog_to_update.gender = _dog["gender"]
        db.session.commit()
        cache.clear()  # clear cache else unit tests will fail

        return True

    def get_all_dogs():
        return [Dog.json(dog) for dog in Dog.query.all()]

    @cached(cache)
    def get_dog(_id):
        return Dog.json(Dog.query.filter_by(id=_id).first())

    def delete_dog(_id):
        success = Dog.query.filter_by(id=_id).delete()
        db.session.commit()
        return success

    def __repr__(self):
        dog_object = {
            'name': self.name,
            'id': self.id,
            'gender': self.gender,
            'breed': self.breed,
            'weight': self.weight,
            'height': self.height
        }
        return json.dumps(dog_object)
