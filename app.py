from flask import Flask, jsonify, request, Response

from DogModel import *
from settings import *
import json
import uuid

db.create_all()


# GET /dogs
@app.route('/dogs')
def get_dogs():
    dogs = {'dogs': Dog.get_all_dogs()}
    return jsonify(dogs)


# GET /dog
@app.route('/dogs/<string:dog_id>')
def get_dog_by_id(dog_id):
    dog = Dog.get_dog(dog_id)
    if not dog:
        return Response("", status=404, mimetype="application/json")
    else:
        return jsonify(dog)


# DELETE /dog
@app.route('/dog/<string:dog_id>', methods=['DELETE'])
def delete_dog(dog_id):
    if Dog.delete_dog(dog_id):
        response = Response("", status=204)
        return response
    else:
        return Response("", status=404, mimetype="application/json")


# PUT /dog
@app.route('/dog', methods=['PUT'])
def update_dog():
    request_data = request.get_json()

    if valid_dog_object(request_data):
        success = Dog.update_dog(request_data)
        if success:
            return Response("", status=204, mimetype="application/json")
        else:
            return Response("", status=404, mimetype="application/json")

    else:
        response = Response(json.dumps(invalid_object_helper_string), status=400, mimetype='application/json')
        return response


# POST /dog
@app.route('/dog', methods=['POST'])
def add_dog():
    request_data = request.get_json()
    if valid_dog_object(request_data):
        dog_id = str(uuid.uuid4())
        Dog.add_dog(dog_id, request_data['name'], request_data['breed'], request_data['height'], request_data['weight'],
                    request_data['gender'])
        response = Response("", status=201, mimetype='application/json')
        response.headers['Location'] = "/dogs/" + dog_id
        return response
    else:
        response = Response(json.dumps(invalid_object_helper_string), status=400, mimetype='application/json')
        return response


def valid_dog_object(dog_object):
    if "name" in dog_object and "breed" in dog_object and "weight" in dog_object and "height" in dog_object and "gender" in dog_object:
        return True
    else:
        return False


app.run(port=app.config['PORT_NUMBER'])
