Author : Jaco de Beer
Project Description : A simple crud REST setup using Flask

Installation notes

Python version : Python 3.7.0

Assumptions : Python V3 is referenced as python3
              Python PIP is for V3 is references as pip3

Setup

Install dependencies:

pip3 install Flask
pip3 install cachetools
pip3 install Flask-SQLAlchemy

Reasoning
Flask : Microframework used to setup REST api
SQL Alchemy : Simple ORM framework for SQLite.  SQLite used for stateful persistence
Cachetools : Simple decorator for setting up simple memory cache

Additional notes:
A database called database.db will be created on first run
Https server port : 5001

Run:

python3 app.py
