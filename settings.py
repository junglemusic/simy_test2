from flask import Flask
import os

cwd = os.getcwd()
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + cwd + "/database.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PORT_NUMBER'] = 5001
app.config['CACHE_URL'] = ''  # Not currently used
app.config['CACHE_DEFAULT_EXP'] = 90
app.config['CACHE_DEFAULT_MEM_SIZE'] = 100
